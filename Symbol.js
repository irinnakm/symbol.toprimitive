Object.prototype[Symbol.toPrimitive] = function (hint) {
    let result;
    if (hint === 'string') {
        result = this.toString();
        if (result !== undefined && result !== null && result.__proto__ !== String.prototype) {
            result = this.valueOf();
        }
    } else {
        result = this.valueOf();
        if (result !== undefined && result !== null && result.__proto__ !== Number.prototype) {
            result = this.toString();
        } 
    }
    return result;
}

console.log(2 * 4);
console.log(1 & 'l')
console.log(String({hello: 'world'}));
console.log({} + []);
console.log([] + {});
console.log(true + false);
console.log("foo" + +"bar");
console.log("foo"+"bar");
console.log("work" + 12);
console.log((function(){}) + 1);